use bevy::{asset::*, prelude::*, reflect::TypeUuid};

#[derive(Default, Debug, Clone, TypeUuid, serde::Deserialize, Component)]
#[uuid = "39cadc56-aa9c-4543-8640-a018b74b505b"]
pub struct Pattern {
    pub name: String,
    pub color: PatternColor,
    pub blocks: Vec<Vec2>,
}

#[derive(Default, Debug, Clone, serde::Deserialize, Component)]
pub enum PatternColor {
    #[default]
    Red,
    Orange,
    Yellow,
    Lime,
    Green,
    LightBlue,
    Blue,
    Indigo,
    Purple,
}

impl From<&str> for PatternColor {
    fn from(s: &str) -> Self {
        match s {
            "red" => PatternColor::Red,
            "orange" => PatternColor::Orange,
            "yellow" => PatternColor::Yellow,
            "lime" => PatternColor::Lime,
            "green" => PatternColor::Green,
            "light blue" => PatternColor::LightBlue,
            "blue" => PatternColor::Blue,
            "indigo" => PatternColor::Indigo,
            "purple" => PatternColor::Purple,
            _ => PatternColor::Red,
        }
    }
}

impl Into<[u8; 4]> for PatternColor {
    fn into(self) -> [u8; 4] {
        match self {
            PatternColor::Red => [235, 71, 111, 255],
            PatternColor::Orange => [250, 103, 56, 255],
            PatternColor::Yellow => [255, 210, 51, 255],
            PatternColor::Lime => [197, 245, 61, 255],
            PatternColor::Green => [73, 233, 137, 255],
            PatternColor::LightBlue => [61, 223, 245, 255],
            PatternColor::Blue => [51, 145, 255, 255],
            PatternColor::Indigo => [93, 89, 255, 255],
            PatternColor::Purple => [120, 61, 245, 255],
        }
    }
}

#[derive(Default)]
pub struct PatternLoader;

impl AssetLoader for PatternLoader {
    fn load<'a>(
        &'a self,
        bytes: &'a [u8],
        load_context: &'a mut LoadContext,
    ) -> BoxedFuture<'a, Result<(), anyhow::Error>> {
        Box::pin(async move {
            let input = String::from_utf8(bytes.to_vec())?;
            let asset = Pattern::from_emoji(input);
            load_context.set_default_asset(LoadedAsset::new(asset));
            Ok(())
        })
    }

    fn extensions(&self) -> &[&str] {
        &["block"]
    }
}

impl Pattern {
    pub fn from_emoji(input: impl ToString) -> Self {
        // split at first newline
        let input = input.to_string();
        let (name, rest) = input.split_once('\n').unwrap();
        let (color, pattern) = rest.split_once('\n').unwrap();
        let mut blocks = Vec::<Vec2>::default();
        let mut cur = Vec2::ZERO;
        pattern.to_string().chars().for_each(|c| {
            match c {
                '⬛' => {
                    cur.x += 1.0;
                }
                '⬜' => {
                    blocks.push(cur);
                    cur.x += 1.0;
                }
                '\n' => {
                    cur.x = 0f32;
                    cur.y -= 1.0;
                }
                e => warn!("unrecognized char \"{}\" in pattern", e),
            };
        });
        let c = PatternColor::from(color);
        Self {
            name: name.into(),
            color: c,
            blocks,
        }
    }
}
